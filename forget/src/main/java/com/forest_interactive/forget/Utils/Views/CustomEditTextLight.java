package com.forest_interactive.forget.Utils.Views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.widget.EditText;

import com.forest_interactive.forget.Utils.Constant;

/**
 * Created by FIN_MULTIMEDIA on 10/10/2016.
 */

public class CustomEditTextLight extends AppCompatEditText implements Constant {

    public CustomEditTextLight(Context context, AttributeSet attrs) {
        super(context, attrs);

        Typeface tp = Typeface.createFromAsset(context.getAssets(), FONT_LIGHT);
        this.setTypeface(tp);
    }
}
